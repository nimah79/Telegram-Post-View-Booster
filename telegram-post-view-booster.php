<?php

/*
 * Telegram Post View Booster
 * By NimaH79
 * NimaH79.ir
 */

$post_url = 'https://t.me/durov/77'; // Change this

$proxies_file = __DIR__.'/proxies.txt'; // Change this

$proxies = explode("\n", file_get_contents($proxies_file));

$gecko = 1;
$mozilla = 0;

foreach ($proxies as $proxy) {
    $user_agent = 'User-Agent: Mozilla/5.'.$mozilla.'(X11; Linux x86_64; rv:52.0) Gecko/'.$gecko.' Firefox/52.'.$mozilla;
    $mozilla++;
    $gecko++;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
    curl_setopt($ch, CURLOPT_URL, $post_url.'?embed=1');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_PROXY, trim($proxy));
    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);

    $response = curl_exec($ch);

    if ($response === false) {
        echo 'bad proxy'.PHP_EOL;
        curl_close($ch);
        continue;
    }

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, ['_rl' => '1']);
    curl_exec($ch);

    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, null);

    preg_match('/data-view="(\w+)"/', $response, $match);

    curl_setopt($ch, CURLOPT_URL, 'https://t.me/v/?views='.$match[1]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['X-Requested-With: XMLHttpRequest']);

    $response = curl_exec($ch);

    if ($response === false) {
        echo 'Bad response'.PHP_EOL;
    } else {
        echo 'OK'.PHP_EOL;
    }

    curl_close($ch);
}
